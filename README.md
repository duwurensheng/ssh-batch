# ssh-batch
运维工具，批量连接ssh并执行命令  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0407/231722_75d77b3d_5176630.png "ssh-batch.png")  
需要注意的是如果想要批量输入用户和密码那么格式如下：  
root,123456,127.0.0.1  
root,123456,127.0.0.1  
root,123456,127.0.0.1  
同时开启从列表中解析用户和密码  
